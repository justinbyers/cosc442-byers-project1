package edu.towson.cis.cosc442.project1.monopoly;

// TODO: Auto-generated Javadoc
/**
 * The Interface IOwnable.
 */
public interface IOwnable {

	/**
	 * Gets the label.
	 *
	 * @return the label
	 */
	String getLabel();

	/**
	 * Apply action.
	 */
	void applyAction();

	/**
	 * Gets the card type.
	 *
	 * @return the card type
	 */
	int getCardType();

}